var burger = {};
burger.name = "";
burger.patty = "";
burger.bun = "";
burger.cheese = "none";
burger.topping = "none";
burger.diytopping = "none";

var options = [];
options.push({
  "key": "patty",
  "label": "Pick a Patty",
  "list": ["Sven’s burger (kid-size, just meat)", "Chris’ Beef special", "Double it!", "Halloumi (v)", "Black bean (v)", "Storebought vegetarian (v)", "Falafel (vg)", "Portabella (Vg)"]
});
options.push({
  "key": "bun",
  "label": "Pick a Bun",
  "list": ["Homemade Brioche", "Bacon-onion brioche", "Sesame white", "White"]
});
options.push({
  "key": "cheese",
  "label": "Get Cheesy",
  "list": ["Cheddar", "Gouda", "Gruyere"]
});
options.push({
  "key": "topping",
  "label": "Toppings (warm)",
  "list": ["Grilled Onions", "Bacon", "Grilled mushrooms"]
});
options.push({
  "key": "diytopping",
  "label": "DIY Toppings (cold)",
  "list": ["crisp Iceberg lettuce", "crisp Romaine", "sliced jalapenos", "sliced onion", "pickles", "sliced tomat"]
});

var colors = [
  "#0080FF",
  "#FFBF00",
  "#74DF00",
  "#FFFF00",
  "#FA5858",
  "#2ECCFA",
  "#81BEF7",
  "#FA58F4",
  "#81F7D8",
  "#FAAC58",
  "#2EFE64",
  "#BCA9F5",
  "#F79F81",
  "#00FFFF"
];
var html = "";
var colorNumber = 0;
options.forEach(function(option){
  html += '<div class="tile-area" data-key="' + option.key + '" data-label="' + option.label + '">';
  html += '<h3><span class="description">' + option.label + '</span><span class="selection"></span></h3>';
  option.list.forEach(function(value){
    html += '<div class="tile" style="background-color: ' + colors[colorNumber] + ';">' + value + '</div>';
    colorNumber = (colorNumber + 1) % colors.length;
  });
  html += '</div>';
});
$('#order').append(html);
