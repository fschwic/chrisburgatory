if (window.location.hash == '#wantburger') {
  $('#imchris').css('visibility', 'hidden');
}
console.log(window.location.hash);

$('#imchris').on('click', function() {
  $('#start').css('visibility', 'hidden');
  $('#chris').css('visibility', 'visible');
  iAmChris();
});

$('#wantburger input').on('click', function(e) {
  e.stopPropagation();
});

$('#wantburger').on('click', function() {
  var myname = $('#wantburger input').val();
  console.log(myname);
  if (myname) {
    $('#start').css('visibility', 'hidden');
    $('#order').css('visibility', 'visible');
    iWantBurger(myname);
    burger.name = myname;
  }
});

$('.tile-area h3').on('click', function() {
  //$(this).parent().find('.tile').css('display', 'block');
  toggleTiles($(this).parent());
});

function toggleTiles(area){
  area.find('.tile').toggleClass('away');
}

$('.tile').on('click', function() {
  var selection = $(this).text();
  console.log(selection);
  var area = $(this).parent();
  console.log(area.data('key'));
  burger[area.data('key')] = selection;

  $(this).toggleClass('selected');
  area.toggleClass('selected');
  area.find('h3 .selection').text(selection);
  //area.find('.tile').css('display', 'none');

  placeOrder(burger);
});

function showToChris(burger) {
  $('#' + burger.name).remove();
  var html = '<div class="order" id="' + burger.name + '"><p><span class="time">' + burger.time + '</span>, ';
  html += '<b>' + burger.name + '</b>: ';
  html += burger.bun + ', ' + burger.patty + ', ' + burger.cheese + ', ' + burger.topping + '.</p>';
  html += '<div class="button done">DONE</div>';
  html += '</div>';
  $('#chris').append(html);

  $('#' + burger.name + ' .button').on('click', function() {
    $('#' + burger.name).remove();
    localStorage.removeItem(burger.name);
  });
}

/* ************************************************** */

var ws;
var myName;
var connectionRetryWaitTime = 1000;

function connect(name) {
  ws = new WebSocket("ws://" + config.hostname + ":" + config.port);
  ws.onopen = function(event) {
    ws.send('{ "name" : "' + name + '" }');
  };
  return ws;
}
function iAmChris() {
  myName = "Chris";
  ws = connect(myName);

  ws.onmessage = function(event) {
    console.log(event.data);

    var burger = JSON.parse(event.data);
    if (burger.name) {
      var date = new Date();
      var time = date.getHours() + ":" + date.getMinutes();

      // Store localy
      burger.time = time;
      localStorage.setItem(burger.name, JSON.stringify(burger));

      showToChris(burger);
    }

  }

  ws.onclose = function(closeEvent) {
    console.log(closeEvent);
    var msg = "The connection of Chris was closed ";
    if (closeEvent.wasClean) {
      msg += "clean ";
    } else {
      msg += "unclean ";
    }
    msg += "because of \"" + closeEvent.reason + "\" ";
    msg += "with error code " + closeEvent.code + ".";
    console.log(msg);

    setTimeout(function() {
      console.log("Reconnecting " + myName);
      connect(myName)
    }, connectionRetryWaitTime);
  }

  ws.onerror = function(event) {
    console.log(event);
  }

  // Load previously not done burgers.
  for (var i = 0; i < localStorage.length; i++) {
    var burger = JSON.parse(localStorage.getItem(localStorage.key(i)));
    showToChris(burger);
  }
}

function iWantBurger(myName) {
  ws = connect(myName);

  ws.onmessage = function(event) {
    console.log(event.data);
  }

  ws.onclose = function(closeEvent) {
    console.log(closeEvent);
    var msg = "The connection of " + myName + " was closed ";
    if (closeEvent.wasClean) {
      msg += "clean ";
    } else {
      msg += "unclean ";
    }
    msg += "because of \"" + closeEvent.reason + "\" ";
    msg += "with error code " + closeEvent.code + ".";
    console.log(msg);
    alert(msg);
  }

  ws.onerror = function(event) {
    console.log(event);
    alert(JSON.stringify(event));
  }
}

function placeOrder(burger) {
  ws.send(JSON.stringify(burger));
}
