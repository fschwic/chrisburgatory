/* ******************************************************\
 * https://github.com/websockets/ws                     *
\****************************************************** */
"use strict";

const serverPort = 3000,
  http = require("http"),
  express = require("express"),
  app = express(),
  server = http.createServer(app),
  WebSocket = require("ws"),
  websocketServer = new WebSocket.Server({
    server
  });

var servant;

//when a websocket connection is established
websocketServer.on('connection', (ws) => {
  //send feedback to the incoming connection
  ws.send('{ "message" : "ok, connected"}');

  //when a message is received
  ws.on('message', (msgString) => {
    var message = JSON.parse(msgString);
    console.log(message);

    if (message.name === "Chris") {
      // this is the servant calling
      servant = ws;
      ws.send('{ "message" : "You are the servent, now." }');
    } else {
      // a request for the servant
      if (servant) {
        servant.send(msgString, {}, () => {
          console.log("Placed the order");
          ws.send(' { "message" : "ok, placed order" }');
        });
      } else {
        ws.send('{ "message" : "ERROR: There is no servant, currently." }');
      }
    }

    /*
    //for each websocket client
    websocketServer
      .clients
      .forEach(client => {
        //send the client the current message
        client.send(`{ "message" : "${message}" }`);
      });
    */
  });
});

//start the web server
server.listen(serverPort, () => {
  console.log(`Websocket server started on port ` + serverPort);
});
